<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Buku;
use Closure;

class BukuController extends Controller
{
    public function index(){
        $buku = Buku::all();

        return response()->json([
            'status' => true,
            'data' => $buku,
            'error' => null
        ]);
        // header("Access-Control-Allow-Origin: *");
        // $headers = [
        //     'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
        //     'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin'
        // ];
        // if($request->getMethod() == "OPTIONS") {
        //     // The client-side application can set only headers allowed in Access-Control-Allow-Headers
        //     return Response::make('OK', 200, $headers);
        // }

        // $response = $next($request);
        // foreach($headers as $key => $value)
        //     $response->header($key, $value);
        // return $response;
    }

    public function create(Request $request){
        /*
            Validator::make(data array, rule array)
        */
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:App\Buku',
            'konten' => 'required'
        ]);

        if($validator->fails()) 
            return response()->json([
                'status' => false,
                'data' => null,
                'error' => $validator->errors()
            ], 400);
            
        $buku = new Buku;
        $buku->title = $request->title;
        $buku->konten = $request->konten;

        $buku->save();
        
        return response()->json([
            'status' => true,
            'data' => $buku,
            'error' => null
        ], 201);
    }

    public function update(Request $request, $id){
        $title = $request->title;
        $konten = $request->konten;

        $validator = Validator::make($request->all(),[
            'title' => 'required|Unique:App\Buku,title,'.$id.',id',
            'konten' => 'required'
        ]);

        if($validator->fails()) 
            return response()->json([
                'status' => false,
                'data' => null,
                'error' => $validator->errors()->first()
            ], 400);

        $buku = Buku::find($id);
        $buku->title = $title;
        $buku->konten = $konten;

        $buku->save();
        
        return response()->json([
            'status' => true,
            'data' => $buku,
            'error' => null
        ]);
    }

    public function delete($id){
        $buku = Buku::find($id);

        if(!$buku) 
            return response()->json([
                'status' => false,
                'data' => null,
                'error' => "Id tidak di temukan!"
            ], 400);

        $buku->delete();

        return response()->json([
            'status' => true,
            'data' => 'Data telah berhasil terhapus',
            'error' => null
        ]);
        
    }
}
