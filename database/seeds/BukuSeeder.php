<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 0; $i <= 50; $i++){
            DB::table('buku')->insert([
                'title' => $faker->sentence(3),
                'konten' => $faker->text(100),
                'created_at' => now(),
            ]);
        }
    }
}
